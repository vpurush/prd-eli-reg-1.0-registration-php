$(function () {
	
  var $sections = $('.form-section');
  var $tab = $('.step');
  function navigateTo(index) {
    // Mark the current section with the class 'current'
   
    $sections 
      .removeClass('current')
      .eq(index)
      .addClass('current');

    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
	 $('.form-navigation .back').toggle(index <= 0);
    var atTheEnd = index >= $sections.length - 1;
    var beforeEnd = index >= $sections.length - 2;
   
    $('.form-navigation .next').toggle(!atTheEnd);
	 $('.form-navigation .next').toggle(!beforeEnd);
    $('.form-navigation .complete').toggle(beforeEnd && !atTheEnd);
    $('.form-navigation .submit').toggle(atTheEnd);
  }
  function addTo(index) {
    console.log(index);
    $tab
      .eq(index)
      .addClass('done');
  }
  function removeTo(index) {
    console.log(index);
      $tab
      .eq(index)
      .removeClass('done');
  
  }
  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.form-navigation .previous').click(function () {
    navigateTo(curIndex() - 1);
    $('.next').prop("disabled", false);
    removeTo(curIndex() + 1);
  });

  // Next button goes forward iff current block validates
  $('.form-navigation .next').click(function () {
    $('.demo-form').parsley().whenValidate({
      group: 'block-' + curIndex()
    }).done(function () {
      //fixStepIndicator(curIndex());
      navigateTo(curIndex() + 1);
      $('.next').prop("disabled", true);
      addTo(curIndex());
    });
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function (index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
  });
  navigateTo(0); // Start at the beginning
  addTo(0);
  
  $(document).ready(function(){
$('.input').keypress(function(e){
if(e.keyCode==13)
$('.next').click();
});
$(document).keypress(function(e){
if(e.keyCode==13)
$('.next').click();
});
});
});



