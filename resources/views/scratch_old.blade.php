<!DOCTYPE html>
<html lang="en" class="codepen">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel="stylesheet" href="public/css/style.css">
  <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
   <style type="text/css">
html
{
  background-image:url(public/img/bg-3.png);
  
}
*, *:before, *:after {
  box-sizing: inherit;
}
.scratchpad{
    width: 320px;
    height: 200px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    border-radius: 6%;
    margin:0 auto;
}


.scratch-container {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  width:100%;
}


  </style>
  
  
</head>

<body>
  <div id="regForm1">
    <div class="Absolute-Center">
      <div class="row">
        <div class="col-md-12 scratchtextalign">
          <h3 style="font-weight: 600;">scratch</h3>
            <h1 style="margin-bottom: 50px; margin-top: 10px;">TO WIN</h1>
        </div>
      </div>
    </div>
    <div class="scratch-container scrathalign">
<div id="promo" class="scratchpad aligncard"></div>
</div>
  </div>

  <div class="fix"><img src="public/img/logo-small.png" class="Absolute-Center" /></div>
  <!-- partial -->
 <script src="public/js/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript" src="public/js/wScratchPad.min.js"></script>

<script type="text/javascript">

// var promoCode = '';
// var bg1 = 'public/img/win.png';
// var bg2 = 'public/img/loss.png';
// var bgArray= [ bg1, bg2 ],
// selectBG = bgArray[Math.floor(Math.random() * bgArray.length)];
// console.log("selectBG",selectBG);
$(document).ready(function(){
var selecteddata= parseInt(localStorage.getItem("winingkey"));
if(selecteddata==1){//win
selectBG = 'public/img/win.png';
}else{
  selectBG = 'public/img/loss.png';
}
console.log("selectBG localStorage",selecteddata,selectBG);

$('#promo').wScratchPad({
    size : 70,       
    bg:  selectBG,
  realtime:true,
    fg: 'public/img/card-1.png',
    'cursor': 'public/img/coin.png") 5 5, default',
    scratchMove: function (e, percent) {
    console.log("success",e, percent)
        if(percent > 50) {
         this.clear();
        }
        localStorage.removeItem("winingkey")//remove set item
        //redirect to next page
         setTimeout(function() {
       window.location.href = "final";
      }, 10000);
       //redirect to next page end
      }
 });
});
</script>

</body>

</html>
