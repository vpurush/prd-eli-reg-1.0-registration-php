<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('index');
});
Route::get('rules', function () {
    return view('rules');
});
Route::get('registerform', function () {
    return view('registerform');
});
Route::get('card', function () {
    return view('card');
});
// Route::get('otp', function () {
//     return view('otp');
// });
// Route::get('scratch', function () {
//     return view('scratch');
// });
// Route::get('final', function () {
//     return view('final');
// });


Route::post('register','CobrandedController@register');


// Route::get('remaindersmsemail','CobrandedController@remaindersmsemail');
// Route::get('participantemail','CobrandedController@participantemail');





// Route::get('participantssms','CobrandedController@participantssms');
// Route::post('playscratch','CobrandedController@playscratch');

