-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2020 at 05:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cobranded_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `md_max_play_count`
--

CREATE TABLE `md_max_play_count` (
  `pk_max_play_id` int(11) NOT NULL,
  `play_count` int(11) NOT NULL,
  `play_date` varchar(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_max_play_count`
--

INSERT INTO `md_max_play_count` (`pk_max_play_id`, `play_count`, `play_date`, `status`) VALUES
(1, 3, '11/05/2020', 0),
(2, 1, '11/06/2020', 0),
(3, 1, '11/09/2020', 0),
(4, 3, '11/10/2020', 0),
(5, 3, '11/11/2020', 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_prize_details`
--

CREATE TABLE `md_prize_details` (
  `pk_prize_id` int(11) NOT NULL,
  `prize_amount` int(10) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_prize_details`
--

INSERT INTO `md_prize_details` (`pk_prize_id`, `prize_amount`, `status`) VALUES
(1, 25, 0),
(2, 75, 0),
(3, 150, 0);

-- --------------------------------------------------------

--
-- Table structure for table `md_prize_logic_info`
--

CREATE TABLE `md_prize_logic_info` (
  `pk_prize_logic_id` int(11) NOT NULL,
  `prize_date` varchar(20) NOT NULL,
  `nth_player_should_won` int(11) NOT NULL,
  `starttime` varchar(11) NOT NULL,
  `endtime` varchar(11) NOT NULL,
  `display_time_between` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_prize_logic_info`
--

INSERT INTO `md_prize_logic_info` (`pk_prize_logic_id`, `prize_date`, `nth_player_should_won`, `starttime`, `endtime`, `display_time_between`, `status`) VALUES
(1, '11/05/2020', 5, '13:00', '13:30', '10:30 AM-1:00 PM', 0),
(2, '11/06/2020', 5, '10:00', '16:00', '10AM-4PM', 0),
(3, '11/09/2020', 5, '10:00', '16:00', '10AM-4PM', 0),
(4, '11/10/2020', 5, '12:00', '16:00', '12PM-4PM', 0),
(5, '11/11/2020', 10, '11:00', '13:00', '11AM-1PM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prize_master`
--

CREATE TABLE `prize_master` (
  `pk_prize_master_id` int(11) NOT NULL,
  `fk_prize_id` int(11) NOT NULL,
  `prize_status` int(11) NOT NULL COMMENT '1-Given,0-Not given',
  `prize_date` varchar(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` varchar(11) NOT NULL,
  `updated_time` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prize_master`
--

INSERT INTO `prize_master` (`pk_prize_master_id`, `fk_prize_id`, `prize_status`, `prize_date`, `status`, `created_at`, `updated_at`, `updated_date`, `updated_time`) VALUES
(1, 1, 1, '11/05/2020', 0, '2020-11-05 18:01:31', '2020-11-05 07:31:31', '11/05/2020', '13:01'),
(2, 1, 0, '11/05/2020', 0, '2020-11-05 14:19:14', '2020-11-03 03:32:13', '', ''),
(3, 1, 0, '11/05/2020', 0, '2020-11-05 14:19:14', '2020-11-03 22:17:51', '', ''),
(4, 2, 0, '11/05/2020', 0, '2020-11-05 14:19:14', '2020-11-05 02:59:42', '', ''),
(5, 1, 0, '11/06/2020', 0, '2020-11-02 14:28:03', '0000-00-00 00:00:00', '', ''),
(6, 1, 0, '11/06/2020', 0, '2020-11-02 14:28:03', '0000-00-00 00:00:00', '', ''),
(7, 1, 0, '11/09/2020', 0, '2020-11-02 14:28:21', '0000-00-00 00:00:00', '', ''),
(8, 1, 0, '11/09/2020', 0, '2020-11-02 14:28:21', '0000-00-00 00:00:00', '', ''),
(9, 1, 0, '11/10/2020', 0, '2020-11-02 14:28:56', '0000-00-00 00:00:00', '', ''),
(10, 1, 0, '11/10/2020', 0, '2020-11-02 14:28:56', '0000-00-00 00:00:00', '', ''),
(11, 2, 0, '11/10/2020', 0, '2020-11-02 14:30:23', '0000-00-00 00:00:00', '', ''),
(12, 3, 0, '11/11/2020', 0, '2020-11-02 14:30:23', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `report`
-- (See below for the actual view)
--
CREATE TABLE `report` (
`pk_user_id` int(10)
,`isWon` int(10)
,`WinningAmount` int(10)
,`FirstName` varchar(200)
,`LastName` varchar(200)
,`EmailAddress` varchar(200)
,`PhoneNumber` varchar(50)
,`Industry` varchar(300)
,`Company` varchar(300)
,`CardNumber` varchar(10)
,`GamePlayedDateTime` timestamp
,`GamePlayedDate` varchar(20)
,`pk_scratchgame_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_061120`
-- (See below for the actual view)
--
CREATE TABLE `report_061120` (
`pk_user_id` int(10)
,`isWon` int(10)
,`WinningAmount` int(10)
,`FirstName` varchar(200)
,`LastName` varchar(200)
,`EmailAddress` varchar(200)
,`PhoneNumber` varchar(50)
,`Industry` varchar(300)
,`Company` varchar(300)
,`CardNumber` varchar(10)
,`GamePlayedDateTime` timestamp
,`GamePlayedDate` varchar(20)
,`pk_scratchgame_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `scratchgameinfo`
--

CREATE TABLE `scratchgameinfo` (
  `pk_scratchgame_id` int(11) NOT NULL,
  `fk_randomcardnumber` varchar(10) NOT NULL,
  `fk_user_id` int(10) NOT NULL,
  `game_result` int(10) NOT NULL COMMENT '1-Win,0-Loss',
  `fk_prize_id` int(11) NOT NULL,
  `fk_prize_master_id` int(11) NOT NULL,
  `status` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` varchar(20) NOT NULL,
  `created_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `pk_user_id` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phonenumber` varchar(50) NOT NULL,
  `industry` varchar(300) NOT NULL,
  `company` varchar(300) NOT NULL,
  `randomcardnumber` varchar(10) NOT NULL,
  `status` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` varchar(20) NOT NULL,
  `created_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`pk_user_id`, `fname`, `lname`, `email`, `phonenumber`, `industry`, `company`, `randomcardnumber`, `status`, `created_at`, `updated_at`, `created_date`, `created_time`) VALUES
(1, 'James', 'MCCLAIN', 'jmcclain@legendaryvisionconsulting.com', '(302) 898-3399', 'Consulting', 'Legendary Vision Consulting', '3367', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(2, 'Consuelo', 'Calderon', 'consuelo.calderon@latam.com', '(568) 846-1008', 'Airlines', 'LATAM', '1944', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(3, 'Tom', 'Lambert', 'tomalambert@yahoo.com', '(919) 306-4431', 'Event Mkt', 'Godfrey Group', '5658', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(4, 'Madeleine', 'Anderson', 'Madeleine@companyevolution.co.uk', '(788) 054-5187', 'Travel', 'Company Evolution', '6867', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(5, 'Karen', 'Dumbrell', 'karen.dumbrell@btinternet.com', '(775) 222-3333', 'Loyalty consultancy', 'Dumbrell Consulting Ltd.', '7203', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(6, 'Bea', 'Peinador', 'beapg22@hotmail.com', '(554) 484-4733', 'FinÃ¡ncial serviles', 'Mastercard', '5727', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(7, 'Pinelopi', 'Parava', 'pinelopi.parava@aegeanair.com', '(003) 064-4260', 'Airlines', 'Aegean Airlines', '4829', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(8, 'Elizabeth', 'Ryan', 'eryan@barclaycardus.com', '(856) 308-6066', 'Financial services', 'Barclays Bank Delaware', '9901', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(9, 'Jennifer', 'Shilling', 'jennifer.shilling@usbank.com', '(612) 973-2250', 'Financial Services', 'U.S. Bank', '9844', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(10, 'Matthew', 'Harre', 'matthew.harre@umb.com', '(913) 634-9213', 'Credit Card', 'UMB Bank', '5025', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(11, 'Charles', 'Chitty', 'cchitty@unionstreetadvisors.com', '(302) 250-2661', 'Advisory services', 'Union Street Advisors', '9741', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(12, 'Gwen', 'Stokes', 'gwenstokes@clearchannel.com', '(215) 837-0376', 'OOH', 'Clear Channel Airports', '2712', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(13, 'Jennifer', 'Shilling', 'jlshilling@yahoo.com', '(651) 334-6122', 'Financial Services', 'US Bank', '1600', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(14, 'Gradislava', 'Vincent', 'gradislava.vincent@accor.com', '(123) 456-6747', 'Hotels', 'Accor', '2965', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(15, 'Marius', 'Froehlich', 'mfroehlich@merkleinc.com', '(248) 882-6614', 'Digital Marketing', 'Merkle, Inc.', '4346', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(16, 'Matt', 'Cairns', 'mcairns@creditshop.com', '(585) 269-4314', 'Credit cards', 'CreditShop', '8824', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(17, 'Ryan', 'Pearl', 'rpearl@barclaycardus.com', '(314) 761-1011', 'Consumer banking; American Airlines cobrand', 'Barclays US', '5270', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(18, 'Charlotte', 'Bjoraker', 'charlotte.bjoraker@usbank.com', '(763) 639-5281', 'Banking', 'US Bank', '5412', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(19, 'Gradislava', 'Vincent', 'bgradya@gmail.com', '(123) 467-8978', 'Hotels', 'Accor', '3299', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(20, 'Kristin', 'Mader', 'kristin.mader@delta.com', '(612) 418-7650', 'Airline', 'Delta', '9188', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(21, 'Cristina', 'Escamilla', 'cristina.escamilla@aa.com', '(812) 964-9737', 'Aviation', 'American Airlines', '8081', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(22, 'Jad', 'Doumit', 'jad_doumit@hotmail.con', '(888) 888-8888', 'Hospitality', 'AAA', '5309', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(23, 'Paul', 'Spooner', 'paulspooner89@gmail.com', '(123) 333-3333', 'Conferences', 'Ai Events', '8179', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(24, 'Arthur', 'Voss', 'arthur.voss@gmail.com', '(002) 123-4567', 'Hospitality', 'Accor', '8450', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(25, 'Caroline', 'Matzke', 'caroline.matzke@usbank.com', '(763) 843-2310', 'Banking', 'Usbank', '2743', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46'),
(26, 'Arthur', 'Voss', 'arthur.voss@accor.com', '(000) 000-0000', 'Hotel', 'Accor', '1318', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '11-07-2020', '00:46');

-- --------------------------------------------------------

--
-- Structure for view `report`
--
DROP TABLE IF EXISTS `report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report`  AS  select `a`.`fk_user_id` AS `pk_user_id`,`a`.`game_result` AS `isWon`,`d`.`prize_amount` AS `WinningAmount`,`b`.`fname` AS `FirstName`,`b`.`lname` AS `LastName`,`b`.`email` AS `EmailAddress`,`b`.`phonenumber` AS `PhoneNumber`,`b`.`industry` AS `Industry`,`b`.`company` AS `Company`,`b`.`randomcardnumber` AS `CardNumber`,`a`.`created_at` AS `GamePlayedDateTime`,`a`.`created_date` AS `GamePlayedDate`,`a`.`pk_scratchgame_id` AS `pk_scratchgame_id` from (((`scratchgameinfo` `a` left join `userinfo` `b` on((`a`.`fk_user_id` = `b`.`pk_user_id`))) left join `prize_master` `c` on((`a`.`fk_prize_master_id` = `c`.`pk_prize_master_id`))) left join `md_prize_details` `d` on((`a`.`fk_prize_id` = `d`.`pk_prize_id`))) order by `a`.`created_at` desc ;

-- --------------------------------------------------------

--
-- Structure for view `report_061120`
--
DROP TABLE IF EXISTS `report_061120`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_061120`  AS  select `a`.`fk_user_id` AS `pk_user_id`,`a`.`game_result` AS `isWon`,`d`.`prize_amount` AS `WinningAmount`,`b`.`fname` AS `FirstName`,`b`.`lname` AS `LastName`,`b`.`email` AS `EmailAddress`,`b`.`phonenumber` AS `PhoneNumber`,`b`.`industry` AS `Industry`,`b`.`company` AS `Company`,`b`.`randomcardnumber` AS `CardNumber`,`a`.`created_at` AS `GamePlayedDateTime`,`a`.`created_date` AS `GamePlayedDate`,`a`.`pk_scratchgame_id` AS `pk_scratchgame_id` from (((`scratchgameinfo` `a` left join `userinfo` `b` on((`a`.`fk_user_id` = `b`.`pk_user_id`))) left join `prize_master` `c` on((`a`.`fk_prize_master_id` = `c`.`pk_prize_master_id`))) left join `md_prize_details` `d` on((`a`.`fk_prize_id` = `d`.`pk_prize_id`))) where (`a`.`created_date` = '11/06/2020') order by `a`.`created_at` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `md_max_play_count`
--
ALTER TABLE `md_max_play_count`
  ADD PRIMARY KEY (`pk_max_play_id`);

--
-- Indexes for table `md_prize_details`
--
ALTER TABLE `md_prize_details`
  ADD PRIMARY KEY (`pk_prize_id`);

--
-- Indexes for table `md_prize_logic_info`
--
ALTER TABLE `md_prize_logic_info`
  ADD PRIMARY KEY (`pk_prize_logic_id`);

--
-- Indexes for table `prize_master`
--
ALTER TABLE `prize_master`
  ADD PRIMARY KEY (`pk_prize_master_id`);

--
-- Indexes for table `scratchgameinfo`
--
ALTER TABLE `scratchgameinfo`
  ADD PRIMARY KEY (`pk_scratchgame_id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`pk_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `md_max_play_count`
--
ALTER TABLE `md_max_play_count`
  MODIFY `pk_max_play_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `md_prize_details`
--
ALTER TABLE `md_prize_details`
  MODIFY `pk_prize_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `md_prize_logic_info`
--
ALTER TABLE `md_prize_logic_info`
  MODIFY `pk_prize_logic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `prize_master`
--
ALTER TABLE `prize_master`
  MODIFY `pk_prize_master_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `scratchgameinfo`
--
ALTER TABLE `scratchgameinfo`
  MODIFY `pk_scratchgame_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `pk_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
