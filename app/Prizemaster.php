<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Prizemaster extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps=false;
    protected $table = 'prize_master';
    protected $primarykey = 'pk_prize_master_id';
    protected $fillable = array('fk_prize_id', 'prize_status', 'prize_date', 'status', 'created_at', 'updated_at', 'updated_date', 'updated_time');

}