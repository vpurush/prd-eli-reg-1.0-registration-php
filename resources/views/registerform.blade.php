<!DOCTYPE html>
<html lang="en" class="formbg">

<head>
  <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel='stylesheet' href='public/css/parsley.css'>
  <link rel="stylesheet" href="public/css/style.css">
<link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
<style>
  @media screen and ( min-width: 375px ) and ( max-height: 700px ){
.verify {
text-align: center;
font-size: 20px;
font-family: 'Source Sans Pro', sans-serif;
font-weight: 600;
padding: 4px 17px 14px 50px;
background-color: #57a0d3;
}

.rowbg{
background-color: #57a0d3;
}

.btn-back, .btn-back:hover, .btn-back:focus{
margin-top: 0 !important;
}
.btn-next, .btn-next:hover, .btn-next:focus{
margin-top: 0 !important;
}
.listalign {
margin-top: -30px;
}
.buttonbg {
background-color: #57a0d3;
}
.form-section1 {
padding-left: 10px;
/* display: none; */
margin-right: 25px;
}
}
/*.btn-info, .btn-default {
     margin-top: 0; 
}*/


@media (min-width:768px){
.btn-info[disabled]{
  margin-top: 0 !important;
}
.btn-complete{
  margin-top: 0 !important;
}
}

@media (min-width: 414px) and (max-width: 500px){
.btn-complete{
  margin-top: 20px !important;
}
/*.btn-info[disabled]{
  margin-top: 20px !important;
}*/
}
@media (min-width: 375px) and (max-width: 413px) and (max-height: 668px){
.btn-complete{
  margin-top: 0 !important;
}
}
@media (min-width: 375px) and (max-width: 413px) and (min-height: 811px){
.btn-complete{
  margin-top: 20px !important;
}
}
</style>
</head>

<body>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center; margin: -7px 0 0 0;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
  <!-- One "tab" for each step in the form: -->
  <!-- partial:index.partial.html -->
  <div>
    <!-- <form id="regForm" class="demo-form"> -->
      <form id="regForm" class="demo-form" autocomplete="off" action="register" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-section-wrapper formalign" style="display: none;">
        <div class="form-section">
          <input type="text" class="form-control input" id="firstname" name="firstname" required="" 
          placeholder="FIRST NAME" onchange="myFunction()" onkeyup="myFunction()">

          <input type="text" class="form-control input" id="lastname" name="lastname" required="" 
          placeholder="LAST NAME" onchange="myFunction()" onkeyup="myFunction()">
        </div>
         <div class="form-section">
          <input type="email" inputmode="email" class="form-control input" id="email" name="email" required="" placeholder="EMAIL ADDRESS" onchange="myFunction()" onkeyup="myFunction()">
              <input type="tel" class="form-control input" maxlength="15" 
          name="phone" required="" id="phone" placeholder="PHONE NUMBER" onchange="myFunction()" onkeyup="myFunction()">
         </div>
        <div class="form-section">
              <input type="text" class="form-control input" id="industry" name="industry" required="" 
          placeholder="INDUSTRY" onchange="myFunction()" onkeyup="myFunction()">

          <input type="text" class="form-control input" id="company" name="company" required="" 
          placeholder="COMPANY" onchange="myFunction()" onkeyup="myFunction()">
        </div>
          <div class="form-section form-wrapper form-section1 verifyinfoalign">
            <P class="verify">VERIFY YOUR INFO</P>
            <div class="form-wrapper listalign">
            <div class="row top form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">FIRST NAME</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="firstname_app"></p>
              <hr>
            </div>
          </div>
             <div class="row form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">LAST NAME</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="lastname_app"></p>
              <hr>
            </div>
          </div>
             <div class="row form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">EMAIL ADDRESS</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="email_app"></p>
              <hr>
            </div>
          </div>
             <div class="row form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">PHONE NUMBER</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="phone_app"></p>
              <hr>
            </div>
          </div>
            <div class="row form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">INDUSTRY</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="industry_app"></p>
              <hr>
            </div>
          </div>
            <div class="row form-group1">
              <div class="col-md-4 col-xs-6 text-right">
            <label class="label-class">COMPANY</label>
          </div>
            <div class="col-md-8 labelvalue col-xs-6">
              <p class="info" id="company_app"></p>
              <hr>
            </div>
          </div>
        </div>
        </div>

        <div class="form-navigation buttonbg">
        <button type="button" class="back btn btn-info pull-left btn-back" onclick="pageRedirect()">BACK</button>
          <button type="button" class="previous btn btn-info pull-left btn-back">BACK</button>
          <button type="button" class="next btn btn-info pull-right btn-next next" disabled>NEXT</button>
           <button type="button" class="next elem__with_auto btn btn-info pull-right btn-complete complete">COMPLETE</button> 
          <button type="submit" class="btn btn-default pull-right btn-next submit">SUBMIT</button>
          <span class="clearfix"></span>
        </div>
      </div>
    </form>
  </div>
  <div class="fix">
    <img src="public/img/logo-small.svg" class="Absolute-Center" />
  </div>
  <!-- partial -->
 <script src="public/js/jquery.min.js"></script>
  <script src='public/js/parsley.js'></script>
  <script src="public/js/script.js"></script>
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script>
   setTimeout(function(){ $(".form-section-wrapper").css("display","block") }, 500);
    function pageRedirect() {
      window.location.href = "rules";
    }
  
    $(document).on('keypress', '#firstname', function (event) {
      var regex = new RegExp("^[a-zA-Z ]+$");
      var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if (!regex.test(key)) {
        event.preventDefault();
        return false;
      }
    });

    $(document).on('keypress', '#lastname', function (event) {
      var regex = new RegExp("^[a-zA-Z ]+$");
      var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
      if (!regex.test(key)) {
        event.preventDefault();
        return false;
      }
    });

    $("#phone").inputmask({
      "mask": "(999) 999-9999"
    });


    var $sections = $('.form-section');
    function curIndex() {
      return $sections.index($sections.filter('.current'));
    }


    function myFunction() {
      var index = curIndex();
      var x = $sections[index].getElementsByTagName("input");
      var exists = $("input", $sections[index]);
      if (index == 1) { 
        if (x[0].value.length >= 3 && x[1].value.length == 14) {
          $('.next').prop("disabled", false);
        }
      }
      else {
        if (x[0].value.length >= 3 && x[1].value.length >= 3) {
          $('.next').prop("disabled", false);
        }
      }
    }

</script> 
 <!-- append value -->
<script>
  $("input").keyup(function(){
  var selected_id=this.id;
  $('#'+selected_id+'_app').text($('#'+selected_id).val());
});
</script>
<!-- append value END -->
</body>

</html>


