<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel='stylesheet' href='public/css/parsley.css'>
  <link rel="stylesheet" href="public/css/style.css">
  <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
<style>
html
{
  background-image:url(public/img/bg.png);  
}
.imgalign{
      margin-top: -50px;
}

</style>
</head>

<body>

  <div id="regForm">
    <div class="vertical-center">
    <img src="public/img/logo.svg" class="img-responsive imgalign" />
    <a class="startButton" href="rules">
      <button type="button" style="margin-left: 8px !important" class="elem__with_auto previous btn btn-info btn-back">Click here to Start</button>
      <!-- <button class="btn btn-back" style="margin-top: 35px;">Click to start</button> -->
    </a>
  </div>
</div>
  <!-- partial -->
 <script src="public/js/jquery.min.js"></script>
  <script src="public/js/parsley.js">
  </script>
  <script src="public/js/script.js"></script>


</body>

</html>
