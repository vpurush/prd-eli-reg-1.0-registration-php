<!DOCTYPE html>
<html lang="en" class="card-bg">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel='stylesheet' href='public/css/parsley.css'>
  <link rel="stylesheet" href="public/css/style.css">
  <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
  <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
  <style>
  html {
    background-image: url(public/img/bg-5.png);
    background-position: 45%;
}
.topheadalign{
margin-top: -1vh;
}
@media screen and ( min-width: 375px ) and ( max-height: 700px ){
.topheadalign{
margin-top: 0;
}
.cardtextalign {
line-height: 30px;
font-weight: 600;
margin-bottom: 20px;
margin-top: 20px;
text-transform: none;
}
}
@media screen and ( min-width: 414px ) and ( max-height: 896px ){
  .cardalign{
          top: 42% !important;
        }
  html{
    background-image: url(public/img/bg-card.png) !important;
  }
  }
</style>
</head>

<body>
  <div id="regForm" class="welcometext">
    <div class="cardalign">
    <div class="Absolute-Center">
      <div class="row topheadalign">
        <div class="col-md-12"> 
          <h4 class="cardtextalign">Congratulations! You have been approved for the Elite/Ai Co-brand Credit Card</h4>
        </div>
      
      </div>
    
    
  </div>
    <div class="Absolute-Center">
      <div class="cardblock">
        <div class="cardblock__number">
          @if(Session::has( 'cardnumber' ))
    <p class="cardblock__number__black">1234 5678 9012 {{Session::get( 'cardnumber' )}}</p>
  @endif
        </div>
        <div class="cardblock__expdate">
          <p class="cardblock__expdate__black">VALID <br/>THRU</p>
        </div>
        <div class="cardblock__expdate_number ">
          <p class="cardblock_expdate_numbe_black">11/25</p>
        </div>
        <div class="cardblock__name ">
           @if(Session::has( 'username' ))
          <p class="cardblock_name_black">{{Session::get( 'username' )}}</p>
          @endif
        </div>
      </div>
    </div>

         <div class="Absolute-Center">
        <div class="row" style="margin-top: -1vh;">
          <div class="col-md-12">
            <h4 class="cardtextalign" style="font-weight: 500">Please write down the last 4 digits to be used throughout the conference</h4>
          </div>

        </div>

      </div>
  
<div class="Absolute-Center"><a href="http://localhost/elitescratchoff/"><button type="button" 
style="margin-right: 0 !important" class="elem__with_auto previous btn btn-info btn-next">Click Here to Play Scratch to Win</button></a></div>

  </div>
</div>
  <div class="fix"><img src="public/img/logo-small.svg" class="Absolute-Center" /></div>
  <!-- partial -->
 <script src="public/js/jquery.min.js"></script>
  <script src='public/js/parsley.js'>
  </script>
  <script src="public/js/script.js"></script>


</body>

</html>
