<!DOCTYPE html>
<html lang="en" class="linkbg">
  <head>
    <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Elite - COBrand</title>
    <link rel='stylesheet'  href='public/css/bootstrap.css'>
      <link rel='stylesheet' href='public/css/parsley.css'>
      <link rel="stylesheet" href="public/css/style.css">
      <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">

      <style>
      .linkbg {
        background-image: url(public/img/bg-1.png);
        background-repeat: repeat-x;
        background-size: contain;
        background-position: 45%;
    }
    .text-center {
    text-align: center;
}
      .links{
      font-size: 20px;
      color: #fff;
      font-weight: 600;
      line-height: 50px
      }
      .linksrow{
        padding-top: 100px;
      }
      .linksalign{
      margin-top: 5vh;
      }

      @media(min-width: 320px) and (max-width: 374px){
        .links {
            font-size: 14px;
            color: #fff;
            font-weight: 600;
            line-height: 40px;
        }
        .linksrow {
            padding-top: 150px;
        }
      }
      @media(min-width: 375px) and (max-width: 400px){
      .linksalign{
      margin-top: 5vh;
      }
       .linksrow{
        padding-top: 200px;
      }
      .links {
      font-size: 16px;
      color: #fff;
      font-weight: 600;
      line-height: 50px;
      }
      }

       @media(min-width: 400px) and (max-width: 480px){
         .linksrow{
        padding-top: 200px;
      }
    .links {
    font-size: 18px !important;
    color: #fff;
    font-weight: 600;
    line-height: 50px;
}
       }
      </style>
    </head>
    <body>
      <div id="regForm" class="">
            <div class="row linksrow">
              <div class="col-md-12 text-center">
                <img src="public/img/link-logo.svg" class="Absolute-Center" style="height: 45px;" />
              </div>
              <div class="col-md-12 linksalign text-center">
                <a class="links" target="_blank" href="https://www.elitemg.com/field-sales/">On-Site Customer Acquisition and Field Sales</a> <br />
                <a class="links" target="_blank" href="https://www.elitemg.com/marketing-services/">Event Marketing and Experiential Marketing</a> <br />
                <a class="links" target="_blank" href="https://www.mobiletourexperts.com/">Mobile Marketing Tours and Pop-Up Experiences</a> <br />
                <a class="links" target="_blank" href="https://www.elitemg.com/covid-19-compliant-displays/">Compliance:  Training and Mystery Shopping</a> <br />
                <a class="links" target="_blank" href="https://www.elitemg.com/">Corporate Events and Experiences</a> <br />
              </div>
            </div>
          </div>
      <script src="public/js/jquery.min.js"></script>
      <script src='public/js/parsley.js'>
      </script>
      <script src="public/js/script.js"></script>
      
    </body>
  </html>