
<!DOCTYPE html>
<html lang="en" class="codepen">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel='stylesheet' href='public/css/parsley.css'>
  <link rel="stylesheet" href="public/css/style.css">
  <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
<style>
html {
    background-image: url(public/img/bg-1.png);
    background-repeat: repeat-x;
    background-size: contain;
    background-position: 52%;
}
.fix{
  position:fixed;
  bottom:20px;
  margin: auto;
  width: 100%;
}
@media screen and ( min-width: 375px ) and ( max-height: 700px ){
.titlealign {
margin-top: 3vh;
margin-bottom: 15px;
}

.btn-back, .btn-back:hover, .btn-back:focus{
margin-top: 0 !important;
}
}
</style>
</head>

<body>
  <div id="regForm">
    <div class="Absolute-Center">
      <div class="row topalign">
        <div class="col-md-12"> 
          <h4  class="titlealign"><strong>Welcome to the Credit Card Application Simulation</strong></h4>
        </div>
      
      </div>
   
    
  </div>
  <div>
    <div class="row">
      <div class="col-md-12">
        <ul>
          <li>We are Elite Marketing Group; leaders in on-site customer acquisition, field sales and  data capture</li>
          <li>The following exercise is a credit card application simulator where you will apply for a  Co-brand credit card</li>
          <li>The last 4 digits of your card will be used to participate in a fun game and win prizes throughout the conference</li>
          <li>Our idea is to showcase the many ways to engage with customers in the Covid environment and beyond</li>
          <li>Enjoy and Good Luck!</li>
        </ul>
      </div>
    </div>
</div>
<div class="Absolute-Center">
  <button type="button" style="margin-left: 0 !Important" class="previous elem__with_auto btn btn-info btn-back" onclick="pageRedirect()">GET STARTED</button></div>

  </div>
  <div class="fix"><img src="public/img/logo-small.svg" class="Absolute-Center" /></div>
  <!-- partial -->
 <script src="public/js/jquery.min.js"></script>
  <script src='public/js/parsley.js'>
  </script>
  <script src="public/js/script.js"></script>

<script type="text/javascript">
  function pageRedirect() {
window.location.href = "registerform";
}
</script>
</body>

</html>
