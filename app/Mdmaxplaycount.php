<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Mdmaxplaycount extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps=false;
    protected $table = 'md_max_play_count';
    protected $primarykey = 'pk_max_play_id';
    protected $fillable = array('play_count', 'play_date', 'status');

}