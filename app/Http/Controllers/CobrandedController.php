<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Userinfo; //Model
use App\Scratchgameinfo;
use App\Mdmaxplaycount;
use App\Prizemaster;
use Illuminate\Http\Request; //request
use Illuminate\Http\Response; //response
use Session;
date_default_timezone_set('US/Eastern');
class CobrandedController extends Controller
{
    public function register(Request $request)
    {
        $fname = $request->input('firstname');
        $lname = $request->input('lastname');
        $email = $request->input('email');
        $phonenumber = $request->input('phone');
        $industry = $request->input('industry');
        $company = $request->input('company');
        $random_num = mt_rand(1000, 9999); // 4 digit unique number
        $currenttime = date("H:i");
        $currentdate = date('m/d/Y');
        if($phonenumber=="(123) 456-7890"){
         $cardnumbercount =0;
        }else{
        $cardnumbercount = Userinfo::orWhere('email', $email)->orWhere('phonenumber', $phonenumber)->count();
         }
        if ($cardnumbercount == 0)
        {
            $insertuserinfo = Userinfo::create(array(
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'phonenumber' => $phonenumber,
                'industry' => $industry,
                'company' => $company,
                'randomcardnumber' => $random_num,
                'created_date' => $currentdate,
                'created_time' => $currenttime
            ));
            $user_id = $insertuserinfo['id'];
            // self::sms($phonenumber,$random_num);
            self::sendemailtouser($email,$random_num);
            return redirect('card')->with('cardnumber', $random_num)->with('username', $fname . ' ' . $lname);
        }
        else
        { //user exist
            return redirect('http://localhost/elitescratchoff/');
        }
    }
    // public function playscratch(Request $request)
    // {
    //     $currenttime = date("H:i");
    //     $currentdate = date('m/d/Y');
    //     // $currentdate = "11/05/2020";
    //     // var_dump($currentdate);exit();
    //     $cardnumber = $request->input('cardnumber');
    //     $cardnumbercount = Userinfo::where('randomcardnumber', $cardnumber)->count();
    //     if ($cardnumbercount == 0)
    //     {
    //         return response()->json(array(
    //             'returncode' => 201,
    //             'message' => 'Invalid card number',
    //         ));
    //     }
    //     else
    //     {
    //         $cardnumbermaxcount = Scratchgameinfo::where('fk_randomcardnumber', $cardnumber)->where('created_date', $currentdate)->count();
    //         $maxdaydata = Mdmaxplaycount::where('play_date', $currentdate)->first();
    //         $maxdaycount = $maxdaydata['play_count'];
    //         if ($cardnumbermaxcount >= $maxdaycount)
    //         {
    //             return response()->json(array(
    //                 'returncode' => 201,
    //                 'message' => 'Your maximum limit exceeded for the day',
    //             ));
    //         }
    //         else
    //         {
    //             //winning logic
    //             $logiccheck = $cardnumbermaxcount + 1; //total count + 1
    //             if ($logiccheck % 3 == 0)
    //             { //winning logic (div by 3)
    //                 $alredywoncount = Scratchgameinfo::where('fk_randomcardnumber', $cardnumber)->where('created_date', $currentdate)->where('game_result', 1)
    //                     ->count();
    //                 if ($alredywoncount == 0)
    //                 { //can win
    //                     //check in Prizemaster
    //                     $prizemastercount = Prizemaster::where('prize_status', 0)->where('prize_date', $currentdate)->count();
    //                     if ($prizemastercount != 0)
    //                     { //can win
    //                         $getprizemasterdata = Prizemaster::where('prize_status', 0)->where('prize_date', $currentdate)->get();
    //                         $a = array();
    //                         foreach ($getprizemasterdata as $getprizemasterdata)
    //                         {
    //                             $pk_prize_master_id = $getprizemasterdata['pk_prize_master_id'];
    //                             array_push($a, $pk_prize_master_id);
    //                         }
    //                         $random_keys = array_rand($a, 1); //get 1 randon value
    //                         $selected_price_Id = $a[$random_keys];
    //                         $winingkey = 1; //won                            
    //                     }
    //                     else
    //                     {
    //                         $winingkey = 0; //loss                            
    //                     }
    //                     // check in Prizemaster end                        
    //                 }
    //                 else
    //                 {
    //                     $winingkey = 0; //loss                        
    //                 }
    //             }
    //             else
    //             {
    //                 $winingkey = 0; //loss                    
    //             }
    //             //update to fk_prize_master_id
    //             if ($winingkey == 1)
    //             {
    //                 $fk_prize_master_id = $selected_price_Id;
    //             }
    //             else
    //             {
    //                 $fk_prize_master_id = 0;
    //             }
    //             //update to fk_prize_master_id
    //             $getuser = Userinfo::where('randomcardnumber', $cardnumber)->first();
    //             $userId = $getuser['pk_user_id'];
    //             $insertinfo = Scratchgameinfo::create(array(
    //                 'fk_user_id' => $userId,
    //                 'game_result' => $winingkey,
    //                 'fk_prize_master_id' => $fk_prize_master_id,
    //                 'fk_randomcardnumber' => $cardnumber,
    //                 'created_date' => $currentdate,
    //                 'created_time' => $currenttime
    //             ));
    //             //update to prize master
    //             if ($winingkey == 1)
    //             {
    //                 $updated_at = date("Y-m-d H:i:s");
    //                 $updateinfo = Prizemaster::where('pk_prize_master_id', $fk_prize_master_id)->update(array(
    //                     'prize_status' => 1,
    //                     'updated_at' => $updated_at,
    //                     'updated_date' => $currentdate,
    //                     'updated_time' => $currenttime
    //                 ));
    //             }
    //             //update to prize master end
    //             //winning logic end
    //             return response()->json(array(
    //                 'returncode' => 200,
    //                 'message' => 'success',
    //                 'winingkey' => $winingkey
    //             ));
    //         }
    //     }
    // }
    public function sms($phone_no,$random_num)
    {
     $numbers = array(
            $phone_no
        );
        $message = "Congratulations, you have been approved! These are your last 4 digits ".$random_num.",  play Scratch to Win! https://www.elitemg.com/elitescratchoff";
        $data = array(
            'User' => 'uvaradha',
            'Password' => 'admin123',
            'PhoneNumbers' => $numbers,
            // 'Subject' => 'test subject',
            'Message' => $message,
            'MessageTypeID' => 1
        );
        $curl = curl_init('https://app.eztexting.com/sending/messages?format=xml');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);       
        curl_close($curl);
        echo $response;     
    }
      public function sendemailtouser($email,$random_num){
    $html ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>ELITE</title>
        <style type="text/css">
            @font-face {
                font-family: Helvetica;
                src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot");
                src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot?#iefix") format("embedded-opentype"), url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.woff") format("woff"), url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.ttf") format("truetype");
                font-weight: normal;
                font-style: normal;
            }
            body {
                height: 100% !important;
                margin: 0;
                padding: 0;
                width: 100% !important;
                font-family: Helvetica;
            }
            table {
                border-collapse: separate;
            }
            img,
            a img {
                border: 0;
                outline: none;
                text-decoration: none;
            }
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin: 0;
                padding: 0;
            }
            p {
                margin: 1em 0;
            }
            table,
            td {
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            #outlook a {
                padding: 0;
            }
            img {
                -ms-interpolation-mode: bicubic;
            }
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            img {
                max-width: 100%;
                height: auto;
            }
        </style>
    </head>
    <body style="background-color: whitesmoke;">
        <table bgcolor="white" width="600" style="margin: 0 auto;" bgcolor="#fff">
            <tbody>
                <tr>
                    <td align="center">
                        <table width="600" align="center" mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center" height="42px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="143px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/toplogo.png" style="display: block; line-height: 0; font-size: 0; border: 0; height: 37px;" border="0" alt="image" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h1 style="text-transform: uppercase; color: #57a0d3; font-size: 36px; line-height: 66px; font-family: Helvetica;">
                                            Congratulations
                                        </h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="42px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="143px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/creditcard-02.jpg" style="display: block; line-height: 0; font-size: 0; border: 0; height: 157px;" border="0" alt="image" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="100px"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="font-size: 18px; line-height: 48px; color: #333333; font-family: Helvetica;">
                                            Congratulations, you have been approved!
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="text-transform: uppercase; font-size: 18px; line-height: 48px; color: #57a0d3; font-family: Helvetica;">
                                           These are your last 4 digits '.$random_num.',
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="font-size: 18px; line-height: 48px; color: #333333; font-family: Helvetica;">play Scratch to Win! https://www.elitemg.com/elitescratchoff</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="103px"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="530px">
                                            <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <h2 style="font-size: 18px; line-height: 25px; color: #333333; font-style: italic; font-family: Helvetica; font-weight: 500; letter-spacing: 0.35px;">
                                                            Thank you from Elite Marketing Group, a full-service Experiential Marketing Agency. Thank you to Ai Events and our Sponsor Vonality for the prizes.
                                                        </h2>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="55px"></td>
                                </tr>
                                <tr>
                                    <td height="25px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/eventlogos.png" style="display: block; line-height: 0; font-size: 0; border: 0; height: 25px;" border="0" alt="image" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="57px"></td>
                                </tr>
                                <tr>
                                    <td height="190px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/footer.png" style="display: block; line-height: 0; font-size: 0; border: 0; width: 600px;" border="0" alt="image" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>'; 
    $url         = 'https://api.sendgrid.com/';
    $emailAry   = array(
        $email,
    );
    $json_string = array(
        'to' => $emailAry,
        // 'category' => 'Scratch to Win'
    );
    $params      = array(
        'api_user' => "jems2013",
        'api_key' => "mobility2013",
        'x-smtpapi' => json_encode($json_string),
        'to' => $emailAry,
        'subject' => "Welcome scratch to win",
        'html' => $html,
        'from' => 'Co-brand@elitemg.com'
    );
    $request     = $url . 'api/mail.send.json';
    $session     = curl_init($request);
    curl_setopt($session, CURLOPT_POST, 1);
    curl_setopt($session, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response1 = curl_exec($session);
    curl_close($session);
    }

    // //participantssms starts
    //  public function participantssms()
    // {

    //     $getdata = Userinfo::select('phonenumber')->get();
    //     $numbers = array();
    //     foreach ($getdata as $getdata)
    //     {
    //         $phonenumber = $getdata['phonenumber'];
    //         array_push($numbers, $phonenumber);
    //     }
    //     var_dump(array_unique($numbers)).'<br>';
    //     // echo "hi";
    //     $message = "Thank you for participating in our application simulation. Don’t forget to play Scratch to Win, daily prizes! https://www.elitemg.com/elitescratchoff";
    //     $data = array(
    //         'User' => 'uvaradha',
    //         'Password' => 'admin123',
    //         'PhoneNumbers' => array_unique($numbers),
    //         // 'Subject' => 'test subject',
    //         'Message' => $message,
    //         'MessageTypeID' => 1
    //     );
    //     $curl = curl_init('https://app.eztexting.com/sending/messages?format=xml');
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($curl, CURLOPT_POST, 1);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //     $response = curl_exec($curl);       
    //     curl_close($curl);
    //     echo $response;     
    // }
    //  //participantssms ends


//Remainder sms & email
     public function remaindersmsemail()
    {

        $getdata = Userinfo::select('phonenumber','email','randomcardnumber')->get();
        foreach ($getdata as $getdata)
        {
            $phone_no = $getdata['phonenumber'];
            $email = $getdata['email'];
            $random_num = $getdata['randomcardnumber'];
            self::remaindersms($phone_no,$random_num);
            self::remainderemail($email,$random_num);
        } 
        // $email="mgaldamez@elitemg.com";
        // // $email="shareeta20@gmail.com";
        // // $phone_no="(516) 474-8166";//5164748166
        // $random_num="7770";
        // self::remainderemail($email,$random_num);
        // // self::remaindersms($phone_no,$random_num);/////////////////// need to comand
        echo "success";  
    }


public function remaindersms($phone_no,$random_num)
    {
     $numbers = array(
            $phone_no
        );
      // $message = "Congratulations, you have been approved! These are your last 4 digits ".$random_num.",  play Scratch to Win! https://www.elitemg.com/elitescratchoff";
     $message = "Thank you for playing Scratch to Win,these are your last 4 digits ".$random_num.".Don't forget to play Scratch to Win,daily prizes! https://www.elitemg.com/elitescratchoff";
        $data = array(
            'User' => 'uvaradha',
            'Password' => 'admin123',
            'PhoneNumbers' => $numbers,
            // 'Subject' => 'test subject',
            'Message' => $message,
            'MessageTypeID' => 1
        );
        $curl = curl_init('https://app.eztexting.com/sending/messages?format=xml');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);       
        curl_close($curl);
        echo $response;     
    }
  public function remainderemail($email,$random_num){
    $html ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>ELITE</title>
        <style type="text/css">
            @font-face {
                font-family: Helvetica;
                src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot");
                src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot?#iefix") format("embedded-opentype"), url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.woff") format("woff"), url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.ttf") format("truetype");
                font-weight: normal;
                font-style: normal;
            }
            body {
                height: 100% !important;
                margin: 0;
                padding: 0;
                width: 100% !important;
                font-family: Helvetica;
            }
            table {
                border-collapse: separate;
            }
            img,
            a img {
                border: 0;
                outline: none;
                text-decoration: none;
            }
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin: 0;
                padding: 0;
            }
            p {
                margin: 1em 0;
            }
            table,
            td {
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            #outlook a {
                padding: 0;
            }
            img {
                -ms-interpolation-mode: bicubic;
            }
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            img {
                max-width: 100%;
                height: auto;
            }
        </style>
    </head>
    <body style="background-color: whitesmoke;">
        <table bgcolor="white" width="600" style="margin: 0 auto;" bgcolor="#fff">
            <tbody>
                <tr>
                    <td align="center">
                        <table width="600" align="center" mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center" height="42px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="143px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/toplogo.png" style="display: block; line-height: 0; font-size: 0; border: 0; height: 37px;" border="0" alt="image" />
                                    </td>
                                </tr>                               
                                <tr>
                                    <td align="center" height="42px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="143px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/creditcard-02.jpg" style="display: block; line-height: 0; font-size: 0; border: 0; height: 157px;" border="0" alt="image" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="100px"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="font-size: 18px; line-height: 48px; color: #333333; font-family: Helvetica;">
                                            Thank you for participating in our application simulation.
                                        </h2>
                                    </td>
                                </tr>                                
                                 <tr>
                                    <td align="center">
                                        <h2 style="text-transform: uppercase; font-size: 18px; line-height: 48px; color: #57a0d3; font-family: Helvetica;">
                                        These are your last 4 digits '.$random_num.'.
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="font-size: 18px; line-height: 48px; color: #333333; font-family: Helvetica;">Don’t forget to play Scratch to Win, we are giving away daily prizes between now and November 11th!</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h2 style="font-size: 18px; line-height: 48px; color: #333333; font-family: Helvetica;">
                                            https://www.elitemg.com/elitescratchoff
                                        </h2>
                                    </td>
                                </tr>  
                                <tr>
                                    <td align="center" height="103px"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="530px">
                                            <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <h2 style="font-size: 18px; line-height: 25px; color: #333333; font-style: italic; font-family: Helvetica; font-weight: 500; letter-spacing: 0.35px;">
                                                            Thank you from Elite Marketing Group, a full-service Experiential Marketing Agency. Thank you to Ai Events and our Sponsor Vonality for the prizes.
                                                        </h2>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="55px"></td>
                                </tr>
                                <tr>
                                    <td height="25px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/eventlogos.png" style="display: block; line-height: 0; font-size: 0; border: 0; height: 25px;" border="0" alt="image" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="57px"></td>
                                </tr>
                                <tr>
                                    <td height="190px" align="center">
                                        <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/footer.png" style="display: block; line-height: 0; font-size: 0; border: 0; width: 600px;" border="0" alt="image" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>'; 
    $url         = 'https://api.sendgrid.com/';
    $emailAry   = array(
        $email,
    );
    $json_string = array(
        'to' => $emailAry,
        // 'category' => 'Scratch to Win'
    );
    $params      = array(
        'api_user' => "jems2013",
        'api_key' => "mobility2013",
        'x-smtpapi' => json_encode($json_string),
        'to' => $emailAry,
        'subject' => "Welcome scratch to win",
        'html' => $html,
        'from' => 'Co-brand@elitemg.com'
    );
    $request     = $url . 'api/mail.send.json';
    $session     = curl_init($request);
    curl_setopt($session, CURLOPT_POST, 1);
    curl_setopt($session, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response1 = curl_exec($session);
    curl_close($session);
    }
// end remainder sms & email


/////////////////////participation email/////////////
  public function participantemail(){
    $html ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width" />
    <title>ELITE</title>
    <style type="text/css">
        @font-face {
            font-family: Helvetica;
            src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot");
            src: url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.eot?#iefix") format("embedded-opentype"),
                url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.woff") format("woff"),
                url("https://www.elitemg.com/elitescratchoff/public/assets/fonts/Helvetica.ttf") format("truetype");
            font-weight: normal;
            font-style: normal;
        }
        body {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
            font-family: Helvetica;
        }

        table {
            border-collapse: separate;
        }

        img,
        a img {
            border: 0;
            outline: none;
            text-decoration: none;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin: 0;
            padding: 0;
        }

        p {
            margin: 1em 0;
        }
        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        #outlook a {
            padding: 0;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        img {
            max-width: 100%;
            height: auto;
        }
    </style>
</head>

<body style="background-color: whitesmoke;">
    <table bgcolor="white" width="600" style="margin: 0 auto;">
        <tbody>
           <tr><td align="center" height="30px">&nbsp;</td></tr>
            <tr>
                <td align="center">
                    <table width="600" align="center" mc:repeatable="castellab" mc:variant="Header" cellspacing="0"
                        cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td height="143px" align="center">
                                    <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/toplogo.png"
                                        style="display:block;line-height:0;font-size:0;border:0;height: 37px;"
                                        border="0" alt="image" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="42px">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td align="center">
                                <table width="550" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                <h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">Thank you for registering for the Co-brand Acquisition and  the Future of Face to Face Marketing.                               </h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">We are excited to  invite everyone to participate in a credit card application simulation created  by Elite Marketing Group.&nbsp; The simulation will showcase instant issuance,  activation, and usage.&nbsp; Everyone will have the opportunity to apply for the  credit card, get approved, and receive the credit card.&nbsp; The credit card  will be used throughout the conference to demonstrate how we can drive  conversion, usage, and ultimately high-quality customers. &nbsp;We encourage  everyone to participate as there is an exciting game and great daily prizes  right up until the end of next week&rsquo;s Co-brand &amp; Travel Reward Cards  Virtual 2020 scheduled for&nbsp;November 10th and 11th.</h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">Additionally, Elite created a QR code to illustrate a  contactless method to engage with customers in today&rsquo;s environment. &nbsp;QR  Codes are a popular and a versatile option that can be used almost anywhere.  &nbsp;A QR code opens the lines of communication to business portals, is a  great way to drive customers to a site, link an app and also allows for  follow-ups via email and/or text.</h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;"><strong><em>This is simply a simulator, we will not be collecting  any Personal Identifiable information.</em></strong></h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">Choose One to begin…&nbsp;</h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">Link:</h2></td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 15px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;"><a href="https://www.elitemg.com/eliteccapp">https://www.elitemg.com/eliteccapp</a></h2></td>
                                    </tr>
                                    <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><h2 style="font-size: 16px;line-height: 25px;color: #333333;font-family: Helvetica;font-weight: 500;letter-spacing: .35px;">Contactless:</h2></td>
                                    </tr>
                                     <tr>
                                      <td align="center" height="20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="left"><img src="https://www.elitemg.com/elitescratchoff/public/assets/qr.png" style="display:block;line-height:0;font-size:0;border:0;height: 200px;"
                                        border="0" alt="image" /></td>
                                    </tr>
                                  </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="55px">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td align="center" height="57px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="190px" align="center">
                                    <img editable="false" mc:edit="image101" src="https://www.elitemg.com/elitescratchoff/public/assets/footer.png"
                                        style="display:block;line-height:0;font-size:0;border:0;width: 600px;"
                                        border="0" alt="image" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>'; 
    $url         = 'https://api.sendgrid.com/';
    $emailAry   = array(
    "shareeta20@gmail.com",
    );
//     $emailAry   = array(
// "alexia.leong@airasiabig.com",
// "alicia.cabrera@caribbean-airlines.com",
// "alychen@visa.com",
// "amy.martin1@ihg.com",
// "ANN.GERSNA@ICFNEXT.COM",
// "anna.elbin@usbank.com",
// "anuja.gagoomal@ihg.com",
// "arthur.voss@gmail.com",
// "Ashley.McGrail@mastercard.com",
// "athina.kakolyri@aegeanair.com",
// "BEA.PEINADOR@MASTERCARD.COM",
// "beapg22@hotmail.com",
// "bgradya@gmail.com",
// "bskelton1990@aol.com",
// "driss.belmadani@gmail.com",
// "ellen.johnson@capitalone.com",
// "emily_k_schwing@yahoo.com",
// "erica.calhounct@gmail.com",
// "esperanza.fernandez@mastercard.com",
// "eugene.kalatsidis@enett.com",
// "gabriela.gomes@latam.com",
// "jack.drew@auriemma.group",
// "jad.doumet@accor.com",
// "jad_doumit@hotmail.com",
// "jason.brusa@united.com",
// "jill@thefirstclub.com",
// "jlshilling@yahoo.com",
// "jnicolo24@gmail.com",
// "jsorento@godfreygroup.com",
// "julie.t.fitzparick@wellsfargo.com",
// "kaila.mcdonnell@gmail.com",
// "kate.morgan@auriemma.group",
// "kelly.passey@accessdevelopment.com",
// "kirill.vycherov@milesandmore.com",
// "larizaga@copaair.com",
// "loay.nour@accor.com",
// "mark.cordes@umb.com",
// "MaxMurrayMedia@gmail.com",
// "mayra.galha@accor.com",
// "mberman@mallettgroup.com",
// "mike.isom@spirit.com",
// "mmore@sponsorsource.com",
// "nauman.moghal@gmail.com",
// "nicol.alletter@accor.com",
// "peter@loyaltyfraudassociation.org",
// "rmandato@sonesta.com",
// "rrodrigues@uatp.com",
// "spenser.marrich-simon@jetblue.com",
// "stan.cochran@deserve.com",
// "stelroco@hotmail.com",
// "steve.arsenault@cheetahdigital.com",
// "todd@consummoconsulting.com",
// "tom.madden@icfnext.com",
// "tomalambert@yahoo.com",
// "toml@godfreygroup.com",
// "will@godfreygroup.com",
// "william.moody@auriemma.group"
//     );
    // var_dump($emailAry);
    $json_string = array(
        'to' => $emailAry,
        // 'category' => 'Scratch to Win'
    );
    $params      = array(
        'api_user' => "jems2013",
        'api_key' => "mobility2013",
        'x-smtpapi' => json_encode($json_string),
        'to' => $emailAry,
        'subject' => "Co-brand Acquisition and the Future of Face to Face Marketing",
        'html' => $html,
        'from' => 'Co-brand@elitemg.com'
    );
    $request     = $url . 'api/mail.send.json';
    $session     = curl_init($request);
    curl_setopt($session, CURLOPT_POST, 1);
    curl_setopt($session, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response1 = curl_exec($session);
    curl_close($session);
    echo "success";
    }
////////////////////END participation email /////////
}

