<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Scratchgameinfo extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps=false;
    protected $table = 'scratchgameinfo';
    protected $primarykey = 'pk_scratchgame_id';
    protected $fillable = array('fk_randomcardnumber', 'fk_user_id', 'game_result', 'fk_prize_master_id', 'status', 'created_at', 'updated_at', 'created_date', 'created_time');

}