<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps=false;
    protected $table = 'userinfo';
    protected $primarykey = 'pk_user_id';
    protected $fillable = array('fname','lname','email','phonenumber','industry','company','randomcardnumber','status','created_at', 'updated_at','created_date', 'created_time');

}