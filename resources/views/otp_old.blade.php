<!DOCTYPE html>
<html lang="en" class="codepen">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Elite - COBrand</title>
  <link rel='stylesheet' href='public/css/bootstrap.css'>
  <link rel='stylesheet' href='public/css/parsley.css'>
  <link rel="stylesheet" href="public/css/style.css">
  <link rel="icon" href="public/img/favicon.jpeg" type="image/gif" sizes="16x16">
  <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
  <style>
html
{
  background-image:url(public/img/bg-5.png);
  
}
</style>
</head>

<body>
  <div id="regForm" class="welcometext">
    <div class="Absolute-Center">
      <div class="row otpalign">
        <div class="col-md-12"> 
          <h3 style="font-weight: 600">Welcome scratch</h3>
          <h1 style="margin-bottom: 50px; margin-top: 10px;">TO WIN</h1>
        </div>
      
      </div>
    
    
  </div>
  <div class="Absolute-Center otpfield">
    <p class="otptext">ENTER LAST 4 DIGITS OF YOUR CARD</p>
  </div>
  <div class="Absolute-Center">
    <section>
        <form autocomplete="off">
          {{ csrf_field() }} <!-- for ajax call -->
            <input id="codeBox1" type="tel" maxlength="1"  onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)">
            <input id="codeBox2" type="tel" maxlength="1"  onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)">
            <input id="codeBox3" type="tel" maxlength="1"  onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)">
            <input id="codeBox4" type="tel" maxlength="1"  onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)">
        </form>
    </section>
  </div>
  <p id="error_msg" style="text-align:center;color:red;"></p>
</div>
  <div class="fix"><img src="public/img/logo-small.png" class="Absolute-Center" alt="" /></div>
  <!-- partial -->
 <script src="public/js/jquery.min.js"></script>
  <script src='public/js/parsley.js'>
  </script>
  <script src="public/js/script.js"></script>
<script type="text/javascript">
function getCodeBoxElement(index) {
  return document.getElementById('codeBox' + index);
}
function onKeyUpEvent(index, event) {
  const eventCode = event.which || event.keyCode;
  if (getCodeBoxElement(index).value.length === 1) {
   if (index !== 4) {
    getCodeBoxElement(index+ 1).focus();
   } else {
    getCodeBoxElement(index).blur();
    // Submit code
    var _token = $('input[name="_token"]').val();//for ajax call
    var otpcode=$("#codeBox1").val()+$("#codeBox2").val()+$("#codeBox3").val()+$("#codeBox4").val()
    console.log('submit code ',otpcode);
     // window.location.href = "scratch.html";
     //ajax call
      $.ajax({
            type: 'POST',
            url: 'playscratch',
            data: {
              _token:_token,
              cardnumber:otpcode
            },
            success: function (data) {
                console.log("data",data.message)
                if(data.returncode==200){
                  console.log('success');
                  localStorage.setItem("winingkey", data.winingkey);
                  window.location.href = "scratch";
                }else{
                $("#error_msg").text(data.message);
                }
            },
            error: function (msg) {
             console.log("error",msg)
              // $("#error_msg").text(msg.message)
            }
         });      //ajax call end
   }
  }
  if (eventCode === 8 && index !== 1) {
   getCodeBoxElement(index - 1).focus();
  }
}
function onFocusEvent(index) {
  for (item = 1; item < index; item++) {
   const currentElement = getCodeBoxElement(item);
   if (!currentElement.value) {
      currentElement.focus();
      break;
   }
  }
}
</script>

</body>

</html>

